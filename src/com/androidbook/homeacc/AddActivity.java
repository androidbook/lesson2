package com.androidbook.homeacc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add);

        Button addButton = (Button)findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText rateName = (EditText)findViewById(R.id.rateName);
                HomeAccApplication app = getHomeAccApplication();
                app.addRate(rateName.getText().toString());
                finish();
            }
        });
    }

    private HomeAccApplication getHomeAccApplication()
    {
        return (HomeAccApplication)getApplication();
    }
}
