package com.androidbook.homeacc;

import android.app.Application;

public class HomeAccApplication extends Application {

    private String rates = "";

    public void addRate(String rate)
    {
        rates += "\n"+rate;
    }

    public String getRates()
    {
        return rates;
    }
}
