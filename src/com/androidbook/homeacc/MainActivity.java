package com.androidbook.homeacc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button showAddScreen = (Button)findViewById(R.id.showAddScreen);
        showAddScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        TextView rateList = (TextView)findViewById(R.id.rateList);
        HomeAccApplication app = getHomeAccApplication();
        rateList.setText(app.getRates());
    }

    private HomeAccApplication getHomeAccApplication()
    {
        return (HomeAccApplication)getApplication();
    }
}
